#!/usr/bin/python3
"""
Crea un programa que haga funciones de calculadora simple. El programa
tendr´a dos funciones (sumar, restar). Cada funci´on aceptar´a dos par´ametros, y
devolver´a su suma o su diferencia. Tambi´en tendr´a un programa principal que lla-
mar´a a esas funciones para sumar primero 1 y 2, y luego 3 y 4, mostrando en cada
caso el resultado en pantalla. A continuaci´on restar´a primero 5 de 6, y luego 7 de
8, mostrando tambi´en los resultados en pantalla.
"""
from sys import argv


def sumar(n1, n2):
    return n1 + n2


def restar(n1, n2):
    return n1 - n2


if __name__ == "__main__":
    print(sumar(1, 2))
    print(sumar(3, 4))
    print(restar(5, 6))
    print(restar(8, 7))

    print(sumar(int(argv[1]), int(argv[2])))
    print(restar(int(argv[1]), int(argv[2])))
